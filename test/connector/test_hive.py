#!/usr/bin/env python3
# -*- coding=utf-8 -*-
import unittest

from bigdata_util.connector.hive_kerberos import HiveKerberos


class TestHive(unittest.TestCase):

    def test_connect(self):
        # Mac系统原因，上无法连接hive的kerberos认证。此库暂不支持hive连接 https://github.com/pythongssapi/python-gssapi/issues/240#issuecomment-784699553
        return
        from bigdata_util.util import BaseConfig, get_absolute_path
        config = BaseConfig(config_file_path=get_absolute_path(__file__, '../config.conf'))
        hive_client = HiveKerberos.instance({
          "principal": config.get('connector.hive_kerberos_principal'),
          "keytab_file_path": get_absolute_path(__file__, config.get('connector.hive_kerberos_keytab_file_path')),
          "host": config.get('connector.hive_kerberos_host'),
          "port": config.get('connector.hive_kerberos_port'),
          "auth_mechanism": config.get('connector.hive_kerberos_auth_mechanism'),
          "kerberos_service_name": config.get('connector.hive_kerberos_service'),
          "database": config.get('connector.hive_kerberos_database')
        }, proxy=config.get('connector.hive_kerberos_proxy'))
        print(hive_client.query('''
          select * from dwd_res_bas_pty_wateruser_info limit 10
        '''))
        hive_client.close()
        pass

    def test_query(self):
        pass


if __name__ == '__main__':
    unittest.main()

