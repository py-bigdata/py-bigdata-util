#!/usr/bin/env python3
# -*- coding=utf-8 -*-

import unittest
from bigdata_util.util.base_config import BaseConfig
from bigdata_util.connector.mysql import MysqlConnector
from bigdata_util.util import get_absolute_path


class MysqlTest(unittest.TestCase):

    def test_run(self):
        cfg = BaseConfig(config_file_path=get_absolute_path(__file__, '../config.conf'))
        mysql_ins = MysqlConnector(
            host=cfg.get('connector.mysql.host'),
            user=cfg.get('connector.mysql.user'),
            port=cfg.get('connector.mysql.port'),
            password=cfg.get('connector.mysql.password'),
            database=cfg.get('connector.mysql.database'),
        )

        data_list = mysql_ins.run_sql_return_plain_json(f'''
            show tables;
        ''')
        self.assertGreaterEqual(len(data_list), 0)
        pass

    def test_run_with_proxy(self):
        cfg = BaseConfig(config_file_path=get_absolute_path(__file__, '../config.conf'))
        mysql_proxy_ins = MysqlConnector(
            host=cfg.get('connector.mysql_proxy.host'),
            user=cfg.get('connector.mysql_proxy.user'),
            port=cfg.get('connector.mysql_proxy.port'),
            password=cfg.get('connector.mysql_proxy.password'),
            database=cfg.get('connector.mysql_proxy.database'),
            proxy=cfg.get('connector.mysql_proxy.proxy'),
        )
        mysql_ins = MysqlConnector(
            host=cfg.get('connector.mysql.host'),
            user=cfg.get('connector.mysql.user'),
            port=cfg.get('connector.mysql.port'),
            password=cfg.get('connector.mysql.password'),
            database=cfg.get('connector.mysql.database'),
        )

        data_list = mysql_proxy_ins.run_sql_return_plain_json(f'''
            show tables;
        ''')
        self.assertGreaterEqual(len(data_list), 0)
        data_list = mysql_ins.run_sql_return_plain_json(f'''
            show tables;
        ''')
        self.assertGreaterEqual(len(data_list), 0)
        pass