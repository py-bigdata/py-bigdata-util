#!/usr/bin/env python3
# -*- coding=utf-8 -*-

import unittest
from bigdata_util.util.base_config import BaseConfig
from bigdata_util.connector import MaxComputeConnector, PostGreConnector
from bigdata_util.util import get_absolute_path, get_logger

logger = get_logger(__file__)


class PostgreTest(unittest.TestCase):

    def test_run_in_file(self):
        cfg = BaseConfig(config_file_path=get_absolute_path(__file__, '../config.conf'))
        odps_ins = MaxComputeConnector(cfg.get('connector.odps'))
        odps_ins.run_sql_in_file(get_absolute_path(__file__, './test_sql_in_file.sql'), {
            'test_param': 1
        })
        pass

    def test_odps_read(self):
        cfg = BaseConfig(config_file_path=get_absolute_path(__file__, '../config.conf'))
        odps_ins = MaxComputeConnector(cfg.get('connector.odps'))
        odps_ins.run_sql_return_plain_json('''
            select * from algtmp_sql_in_file_test_1
        ''')
        pass

    def test_update_archive(self):
        cfg = BaseConfig(config_file_path=get_absolute_path(__file__, '../config.conf'))
        odps_ins = MaxComputeConnector(cfg.get('connector.odps'))
        test_rsc_name = 'py_bigdata_util_test_odps.py'

        if odps_ins.exist_resource(test_rsc_name):
            odps_ins.delete_resource(test_rsc_name)

        odps_ins.update_file_resource(
            test_rsc_name,
            'py',
            __file__
        )
        odps_ins.update_file_resource(
            test_rsc_name,
            'py',
            __file__
        )
        odps_ins.delete_resource(test_rsc_name)
        pass


if __name__ == '__main__':
    unittest.main()
    pass
