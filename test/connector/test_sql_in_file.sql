-- test comment
drop table if exists algtmp_sql_in_file_test_${test_param};
-- 测试注释;
create table algtmp_sql_in_file_test_${test_param}(a bigint, b bigint);

insert into algtmp_sql_in_file_test_${test_param} -- comment0
-- comment1
-- comment2
values(1, 2), (2, 3), (3, 4); -- comment3
