#!/usr/bin/env python3
# -*- coding=utf-8 -*-

import time
import os
import json
from bigdata_util.util.base_config import BaseConfig
from bigdata_util.connector.maxcompute import MaxComputeConnector


class RequestApi:
    def __init__(self):
        pass

    def run(self):
        adcode = '530100'
        dt = '20210726'
        partition = f'dt="{dt}",adcode="{adcode}"'

        cfg = BaseConfig(config_file_path=os.path.join(os.environ['HOME'], 'odps_config.conf'))
        odps_ins = MaxComputeConnector(cfg.get('connector.odps_pub'))
        t = odps_ins.get_table('algtmp_amap_traffic_speed')
        t.delete_partition(partition_spec=partition, if_exists=True)

        data_list = []
        for adcode in ['530102', '530103', '530111', '530112', '530113', '530114', '530115', '530124', '530125', '530126', '530127', '530128', '530129', '530181']:
        # for adcode in ['530102']:
            result = os.popen(f'''
            curl 'https://its.amap.com/judge/area/areaJudgeMin?adcode=530100&linksType=4&id={adcode}' \
  -H 'authority: its.amap.com' \
  -H 'pragma: no-cache' \
  -H 'cache-control: no-cache' \
  -H 'sec-ch-ua: " Not;A Brand";v="99", "Google Chrome";v="91", "Chromium";v="91"' \
  -H 'accept: application/json, text/plain, */*' \
  -H 'x-xsrf-token: 82cc2cde-efad-4775-a79c-636d23d650ac' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36' \
  -H 'sec-fetch-site: same-origin' \
  -H 'sec-fetch-mode: cors' \
  -H 'sec-fetch-dest: empty' \
  -H 'referer: https://its.amap.com/?' \
  -H 'accept-language: zh-CN,zh;q=0.9,en;q=0.8' \
  -H 'cookie: userName=kmsadmin; Kdw4_5279_ulastactivity=ffdd5IzTrLJ8MLJSl9RKjibQnnUERp30b3UyN98TXuDk3wW0sHAH; Kdw4_5279_smile=1D1; _ga=GA1.2.1057376776.1604891211; UM_distinctid=17839a6685b9b4-0f2aa2b1b8143e-1731685c-1aeaa0-17839a6685d8dd; cna=8Y51GfkUekQCASp4S+M0eGHE; xlly_s=1; _uab_collina=162728436087719198395692; isLogined=false; XSRF-TOKEN=82cc2cde-efad-4775-a79c-636d23d650ac; SESSION=NTkyNWNhMTctYWFhOS00YjQyLTkwOTMtZGYyMDVlYjM3MmY1; userName=kmsadmin; l=eBLCZJn7vo2qWO5wBO5w-urza77tPKOfBsPzaNbMiIncC6I2Rz9MlGxQD9-1sGtRR8Xci28e4koJbz2TbePz-PHjj130bGUyopnkCef..; tfstk=cniOB7VUEpB98AwKUVLHlPpu5BNca19TTOwODdRBAoLwSQjdAs0sq0EeNFNw20pd.; isg=BKysi1ot4IFmi8ggbtNWWgMXfYzeZVAPQUOttwbkkNepEWkbLXS1noLnMdmpmYhn' \
  --compressed
            ''').read()
            j = json.loads(result)

            for idx in range(len(j['data']['indices'])):
                data_list.append([
                    str(j['data']['id']),
                    json.dumps(j['data']['indices'][idx])
                ])

            time.sleep(1)

        with t.open_writer(partition=partition, create_partition=True) as writer:
            writer.write(data_list)
        pass


if __name__ == '__main__':
    RequestApi().run()

