#!/usr/bin/env python3
# -*- coding=utf-8 -*-

import unittest
from bigdata_util.util.str_format import lnglat_seq_2_wkt, safe_get_and_not_none


class AngleTest(unittest.TestCase):

    def test_lnglat_seq_2_wkt(self):
        self.assertEqual(lnglat_seq_2_wkt('1,1;2,2;3,3'), 'LINESTRING (1.0 1.0, 2.0 2.0, 3.0 3.0)')
        self.assertEqual(
            lnglat_seq_2_wkt('1,1;2,2;3,3;1,1', 'POLYGON'),
            'POLYGON ((1.0 1.0, 2.0 2.0, 3.0 3.0, 1.0 1.0))'
        )
        self.assertEqual(lnglat_seq_2_wkt('1,1;2,2;3,3', 'linestring'), 'LINESTRING (1.0 1.0, 2.0 2.0, 3.0 3.0)')
        self.assertEqual(lnglat_seq_2_wkt('1,1', 'pOINT'), 'POINT (1.0 1.0)')
        pass

    def test_min_diff_angle(self):
        meta = {
            'a': 1
        }
        self.assertEqual(safe_get_and_not_none(meta, 'a'), 1)
        self.assertEqual(safe_get_and_not_none(meta, 'b'), '')
        pass


if __name__ == '__main__':
    unittest.main()
    pass
